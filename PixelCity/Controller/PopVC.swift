//
//  PopVC.swift
//  PixelCity
//
//  Created by Tobias Biermeier on 8/10/17.
//  Copyright © 2017 Tobias Biermeier. All rights reserved.
//

import UIKit

class PopVC: UIViewController, UIGestureRecognizerDelegate {

    // Outlets
    @IBOutlet weak var popImageView: UIImageView!
    
    // Variable
    var passedImage: UIImage!
    
    // Create data to gain the Image data when the user taps a cell
    func initData(forImage image: UIImage) {
        self.passedImage = image
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        popImageView.image = passedImage
        addDoubleTap()
    }

    // Add Double tap function to dismiss the view controller
    func addDoubleTap() {
         let doubleTap = UITapGestureRecognizer(target: self, action: #selector(screenWasDoubleTapped))
        doubleTap.numberOfTapsRequired = 2
        doubleTap.delegate = self
        view.addGestureRecognizer(doubleTap)
    }
    
    @objc func screenWasDoubleTapped() {
        dismiss(animated: true, completion: nil)
    }
    
}





















