//
//  Constants.swift
//  PixelCity
//
//  Created by Tobias Biermeier on 8/10/17.
//  Copyright © 2017 Tobias Biermeier. All rights reserved.
//

import Foundation

let apiKey = "a822f34e429c1b40b136798d57fae30b"

func flickrUrl(forApiKey key: String, withAnnotation annotation: DroppablePin, andNumberOfPhotos number: Int) -> String {
    return "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(apiKey)&lat=\(annotation.coordinate.latitude)&lon=\(annotation.coordinate.longitude)&radius=1&radius_units=mi&per_page=\(number)&format=json&nojsoncallback=1"
}














