//
//  DroppablePin.swift
//  PixelCity
//
//  Created by Tobias Biermeier on 8/8/17.
//  Copyright © 2017 Tobias Biermeier. All rights reserved.
//

import UIKit
import MapKit

class DroppablePin: NSObject, MKAnnotation {
    
    // Variables
    dynamic var coordinate: CLLocationCoordinate2D
    var identifier: String
    
    init(coordinate: CLLocationCoordinate2D, identifier: String) {
        self.coordinate = coordinate
        self.identifier = identifier
        super.init()
    }

    
    
}























